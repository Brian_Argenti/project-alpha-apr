from django.shortcuts import render, redirect
from .forms import ProjectForm
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_projects(request):
    projects_list = Project.objects.filter(owner=request.user)

    context = {
        "projects_list": projects_list,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project_detail = Project.objects.filter(owner=request.user, id=id)
    task_details = Task.objects.filter(project=id)
    context = {
        "project_detail": project_detail,
        "task_details": task_details,
    }

    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {"form": form}
    return render(request, "projects/create.html", context)
